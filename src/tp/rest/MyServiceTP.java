package tp.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.UUID;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Provider;
import javax.xml.ws.Service;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;
import javax.xml.ws.http.HTTPException;

import tp.model.Animal;
import tp.model.AnimalNotFoundException;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;

@WebServiceProvider
@ServiceMode(value = Service.Mode.MESSAGE)
public class MyServiceTP implements Provider<Source> {
	
    public final static String url = "http://127.0.0.1:8084/";
    
    public static void main(String args[]) {
        Endpoint e = Endpoint.create(HTTPBinding.HTTP_BINDING, new MyServiceTP());
        e.publish(url);
        System.out.println("Service started, listening on " + url);
        // pour arrêter : e.stop();
    }
    
    private JAXBContext jc;
    
    @javax.annotation.Resource(type = Object.class)
    protected WebServiceContext wsContext;
   
    private Center center = new Center(new LinkedList<>(), new Position(49.30494d, 1.2170602d), "Biotropica");
    
    public MyServiceTP() {
        try {
            jc = JAXBContext.newInstance(Center.class, Cage.class, Animal.class, Position.class);
        } catch (JAXBException je) {
            System.out.println("Exception " + je);
            throw new WebServiceException("Cannot create JAXBContext", je);
        }
        // Fill our center with some animals
        Cage usa = new Cage(
                "usa",
                new Position(49.305d, 1.2157357d),
                25,
                new LinkedList<>(Arrays.asList(
                        new Animal("Tic", "usa", "Chipmunk", UUID.randomUUID()),
                        new Animal("Tac", "usa", "Chipmunk", UUID.randomUUID())
                ))
        );
        Cage amazon = new Cage(
                "amazon",
                new Position(49.305142d, 1.2154067d),
                15,
                new LinkedList<>(Arrays.asList(
                        new Animal("Canine", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Incisive", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Molaire", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("De lait", "amazon", "Piranha", UUID.randomUUID())
                ))
        );
        center.getCages().addAll(Arrays.asList(usa, amazon));
    }
    
    public Source invoke(Source source) {
        MessageContext mc = wsContext.getMessageContext();
        String path = (String) mc.get(MessageContext.PATH_INFO);
        String method = (String) mc.get(MessageContext.HTTP_REQUEST_METHOD);
        // determine the targeted ressource of the call
        try {
            // no target, throw a 404 exception.
            if (path == null) {
                throw new HTTPException(404);
            }
            // "/animals" target - Redirect to the method in charge of managing this sort of call.
            else if (path.startsWith("animals")) {
                String[] path_parts = path.split("/");
                switch (path_parts.length){
                    case 1 : // Méthodes effectuant des actions sans id
                        return this.animalsCrud(method, source);
                    case 2 : // Méthodes effectuant des actions en spécifiant l'id
                        return this.animalCrud(method, source, path_parts[1]);
                    case 3 : // Permet de récupérer les infos wolfram d'un animal
                    	if (path_parts[2].equals("wolf")) {
                    		return animalWolfram(method, source, path_parts[1]);
                    	} else {
                    		throw new HTTPException(404);
                    	}
                    default :
                    	throw new HTTPException(404);
                }
            } else if (path.startsWith("find/")) {
            	String[] path_parts = path.split("/");
            	if (path_parts.length == 3) {
            		switch (path_parts[1]) {
            			case "byName" : // Trouver un animal par son nom
            				return findAnimalByName(method, source, path_parts[2]);
            			case "at" : // Trouver d'un animal par position 
            				return findAnimalAtPosition(method, source, path_parts[2]);
            			case "near" : // Trouver des animaux près d'une position
            				return findAnimalNearPosition(method, source, path_parts[2]);
            			default :
            				throw new HTTPException(404);	
            		}
            	} else {
            		throw new HTTPException(404);
            	}
            } else if (path.startsWith("cages")) {
            	String[] path_parts = path.split("/");
            	if (path_parts.length == 1) {
            		return addCage(method, source);
            	} else if (path_parts.length == 2) {
            		return removeCage(method, source, path_parts[1]);
            	} else {
            		throw new HTTPException(404);
            	}
            } else {
                throw new HTTPException(404);
            }
        } catch (JAXBException e) {
            throw new HTTPException(500);
        }
    }
    
    /**
     * Method bound to calls on /cages
     * Add/Delete a cage in the center
     */
    private Source addCage(String method, Source source) throws JAXBException {
    	if ("POST".equals(method)) {
    		Cage cage = this.unmarshalCage(source);
    		center.getCages().add(cage);
    		cage.setResidents(new LinkedList<>());
    		return new JAXBSource(this.jc, this.center);
    	} else if ("DELETE".equals(method)) {
    		Cage cage = this.unmarshalCage(source);
    		Collection<Animal> residents = center.getCages()
    				.stream()
    				.filter(c -> c.equals(cage))
    				.findFirst()
    				.get()
    				.getResidents();
    		residents.removeAll(residents);
    		return new JAXBSource(this.jc, this.center);
    	} else {
    		throw new HTTPException(404);
    	}
	}
    
    /**
     * Method bound to calls on /cages/{name}
     * Delete all residents of a cage
     */
    private Source removeCage(String method, Source source, String cageName) throws JAXBException {
    	 if ("DELETE".equals(method)) {
    		Collection<Animal> residents = center.getCages()
    				.stream().filter(cage2 -> cage2.getName().equals(cageName))
    				.findFirst()
    				.orElseThrow(() -> new HTTPException(404))
    				.getResidents();
    		residents.removeAll(residents);
    		return new JAXBSource(this.jc, this.center);
    	} else {
    		throw new HTTPException(404);
    	}
	}
    
    // Recherche d'un animal sur wolfram
	private Source animalWolfram(String method, Source source, String animal_id) throws JAXBException {
    	if ("GET".equals(method)) {
    		Animal anim = null;
    		
    		try {
				anim = center.findAnimalById(UUID.fromString(animal_id));
			} catch (AnimalNotFoundException e) {
				throw new HTTPException(404);
			}
    		
    		try {
    			String species = anim.getSpecies();
    			URL url = new URL("https://api.wolframalpha.com/v2/query"
    					+ "?input=" + species + "&format=image,plaintext"
    							+ "&output=XML&appid=KR7WRP-HAX4RG5UY9");
    			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    			conn.setDoOutput(true);
    			conn.setRequestMethod("GET");
    			conn.setRequestProperty("Content-Type", "application/xml");
    			OutputStream os = conn.getOutputStream();
    			os.flush();
    			
    			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
    				throw new RuntimeException("Failed : HTTP error code : "
    					+ conn.getResponseCode());
    			}
    			
    			BufferedReader br = new BufferedReader(new InputStreamReader(
    					(conn.getInputStream())));
    		
    			String result = "";
    			String line = "";
    			while ((line = br.readLine()) != null) {
    				result = result + line;
    			}
    			
    			conn.disconnect();
    			return new StreamSource(new StringReader(result));
    		  } catch (MalformedURLException e) {
    			  throw new HTTPException(404);
    		  } catch (IOException e) {
    			  throw new HTTPException(404);
			  }
    	} else {
			throw new HTTPException(405);
		}
	}
	
	/**
     * Method bound to calls on /find/byName/{name}
     * Search an animal with his name
     */
	private Source findAnimalByName(String method, Source source, String animal_name) throws JAXBException {
		if ("GET".equals(method)) {
			Animal animal = this.center.getCages().stream().filter(cage -> 
						cage.getResidents().stream().filter(ani -> ani.getName().equals(animal_name))
						.findFirst().isPresent()
					).findFirst()
			.orElseThrow(() -> new HTTPException(404))
			.getResidents().stream().filter(ani -> ani.getName().equals(animal_name)).findFirst().get();
            			
			return new JAXBSource(this.jc, animal);
		} else {
			throw new HTTPException(405);
		}
	}
	
	/**
     * Method bound to calls on /find/at/{position}
     * Search an animal at position
     */
	private Source findAnimalAtPosition(String method, Source source, String position) throws JAXBException {
    	if ("GET".equals(method)) {
    		String[] pos = position.split(":");
    		Double latitude = Double.parseDouble(pos[0]);
    		Double longitude = Double.parseDouble(pos[1]);
    		Position posit = new Position(latitude, longitude);
			Cage cage = this.center.getCages().stream().filter(cage2 -> 
						cage2.getPosition().equals(posit))
						.findFirst().orElseThrow(() -> new HTTPException(404));
            			
			return new JAXBSource(this.jc, cage);
		} else {
			throw new HTTPException(405);
		}
	}
	
	/**
     * Method bound to calls on /find/near/{position}
     * Search animals near a position
     */
	private Source findAnimalNearPosition(String method, Source source, String position) throws JAXBException{
    	if ("GET".equals(method)) {
    		String[] pos = position.split(":");
    		Double latitude = Double.parseDouble(pos[0]);
    		Double longitude = Double.parseDouble(pos[1]);
    		Center centerRes = new Center();
			this.center.getCages().stream().filter(cage -> 
						cage.getPosition().getLatitude() < latitude + 1 &&
						cage.getPosition().getLatitude() > latitude - 1 &&
						cage.getPosition().getLongitude() < longitude + 1 &&
						cage.getPosition().getLongitude() > longitude - 1)
				.forEach(cage -> centerRes.getCages().add(cage));
            			
			return new JAXBSource(this.jc, centerRes);
		} else {
			throw new HTTPException(405);
		}
	}
	
	/**
     * Method bound to calls on /animals/{something}
     */
    private Source animalCrud(String method, Source source, String animal_id) throws JAXBException {
        if("GET".equals(method)){
            try {
                return new JAXBSource(this.jc, center.findAnimalById(UUID.fromString(animal_id)));
            } catch (AnimalNotFoundException e) {
                throw new HTTPException(404);
            }
        } else if ("POST".equals(method)) {
        	Animal animal = unmarshalAnimal(source);
        	animal.setId(UUID.fromString(animal_id));
            this.center.getCages()
                    .stream()
                    .filter(cage -> cage.getName().equals(animal.getCage()))
                    .findFirst()
                    .orElseThrow(() -> new HTTPException(404))
                    .getResidents()
                    .add(animal);
            return new JAXBSource(this.jc, this.center);
        } else if ("PUT".equals(method)) {
        	Animal animal = unmarshalAnimal(source);
        	animal.setId(UUID.fromString(animal_id));
        	Animal old;
			try {
				old = center.findAnimalById(UUID.fromString(animal_id));
				this.center.getCages()
	                .stream()
	                .filter(cage -> cage.getName().equals(old.getCage()))
	                .findFirst()
	                .orElseThrow(() -> new HTTPException(404))
	                .getResidents()
	                .remove(old);
				
				this.center.getCages()
	                .stream()
	                .filter(cage -> cage.getName().equals(animal.getCage()))
	                .findFirst()
	                .orElseThrow(() -> new HTTPException(404))
	                .getResidents()
	                .add(animal);
			} catch (AnimalNotFoundException e) {
				throw new HTTPException(404);
			}
			
            return new JAXBSource(this.jc, this.center);
            
        } else if ("DELETE".equals(method)) {
        	Animal old;
			try {
				old = center.findAnimalById(UUID.fromString(animal_id));
				this.center.getCages()
	                .stream()
	                .filter(cage -> cage.getName().equals(old.getCage()))
	                .findFirst()
	                .orElseThrow(() -> new HTTPException(404))
	                .getResidents()
	                .remove(old);
				
			} catch (HTTPException e) {
				throw new HTTPException(404);
			} catch (AnimalNotFoundException e) {
				throw new HTTPException(404);
			}
			
            return new JAXBSource(this.jc, this.center);
        } else {
            throw new HTTPException(405);
        }
    }
    /**
     * Method bound to calls on /animals
     */
    private Source animalsCrud(String method, Source source) throws JAXBException {
        if("GET".equals(method)){
            return new JAXBSource(this.jc, this.center);
        }
        else if("POST".equals(method)){
            Animal animal = unmarshalAnimal(source);
            this.center.getCages()
                    .stream()
                    .filter(cage -> cage.getName().equals(animal.getCage()))
                    .findFirst()
                    .orElseThrow(() -> new HTTPException(404))
                    .getResidents()
                    .add(animal);
            return new JAXBSource(this.jc, this.center);
        } else if("PUT".equals(method)) {
        	Animal animal = this.unmarshalAnimal(source);
        	this.center.getCages()
                    .stream()
                    .filter(cage -> cage.getName().equals(animal.getCage()))
                    .forEach(c -> c.getResidents()
                    		.forEach(resident -> {
                    			c.getResidents().remove(resident);
                    			c.getResidents().add(animal);
                    		}));
        	return new JAXBSource(this.jc, this.center);
        } else if("DELETE".equals(method)) {
    		this.center.getCages().forEach(cage -> cage.getResidents().removeAll(cage.getResidents()));
    		return new JAXBSource(this.jc, this.center);
    	} else {
            throw new HTTPException(405);
        }
    }
    private Animal unmarshalAnimal(Source source) throws JAXBException {
        return (Animal) this.jc.createUnmarshaller().unmarshal(source);
    }
    
    private Cage unmarshalCage(Source source) throws JAXBException {
        return (Cage) this.jc.createUnmarshaller().unmarshal(source);
    }
}