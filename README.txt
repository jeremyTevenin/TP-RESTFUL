TP 1 & 2 d'Architectures Distribu�es

Lien du JS Fiddle : http://jsfiddle.net/jeremyTevenin/7ny1Lf5v/

Nous devions fournir les m�thodes suivantes :

PUT 		/animals  				Modifie l'ensemble des animaux
DELETE 		/animals 				Supprime l'ensemble des animaux
POST 		/animals/{animal_id} 	Animal Cr�e l�animal identifi� par {animal_id}
PUT 		/animals/{animal_id} 	Animal Modifie l�animal identifi� par {animal_id}
DELETE 		/animals/{animal_id} 	Supprime l�animal identifi� par {animal_id}
GET 		/find/byName/{name} 	Recherche d'un animal par son nom
GET 		/find/at/{position} 	Recherche d'un animal par position
GET 		/find/near/{position}	Recherche des animaux pr�s d�une position
GET 		/animals/
			{animal_id}/wolf 		R�cup�ration des info. Wolfram d�un animal
GET 		/center/journey/from/
			{position}     			R�cup�ration des info. Du trajet depuis une position GPS jusqu�� votre centre en
									utilisant le service Graphhopper.

									******

Pour tester notre application, vous devez d'abord lancer le fichier MyServiceTP.java puis le client (MyClient.java)
Nous avons g�n�r� les fichiers correspondant aux sc�narios propos� par le client (ces fichiers se trouvent dans le dossier sc�nario).									
									
									******

Nous avons donc r�alis� toutes les m�thodes suivantes pour r�pondre aux attentes du client : 
	 
	/**
	 * Requete de type GET retournant l'ensemble des animaux du centre.
     */
    get_animals() throws JAXBException
    
    /**
	 * Requete de type POST ajoutant un animal dans le centre.
     */
    add_animal(Animal animal) throws JAXBException
    
    /**
	 * Requete de type DELETE supprimant l'ensemble des animaux du centre.
     */
	remove_animals() throws JAXBException
    
	/**
	 * Requete de type PUT modifiant l'ensemble des animaux du centre.
     */
    put_animals(Animal animal) throws JAXBException
        
    /**
	 * Requete de type POST cr�ant l'animal identifi� par l'id.
     */
    public Source add_animal_id(Animal animal, String uuid) throws JAXBException
    
    /**
	 * Requete de type PUT modifiant l'animal identifi� par l'id.
     */
    put_animal_id(Animal animal, String uuid) throws JAXBException
    
    /**
	 * Requete de type DELETE supprimant l'animal identifi� par l'id.
     */
    public Source delete_animal_id(String uuid) throws JAXBException
    
    /**
	 * Requete de type GET recherchant un animal par son nom.
     */
    find_animal_by_name(String name) throws JAXBException
	
    /**
	 * Requete de type GET recherchant un animal par position.
     */
    public Source find_animal_at_position(Position position) throws JAXBException
    
    /**
	 * Requete de type GET retournant l'ensemble des animaux pr�s d'une position.
     */
	find_animal_near_position(Position position) throws JAXBException
    
    /**
	 * Requete de type GET r�cup�rant les informations wolfram d'un animal.
     */
    find_animal_wolf(String uuid) throws JAXBException
	
Nous avons d�cid� d'ajouter deux m�thodes suppl�mentaires pour permettre au client de rajouter/enlever des cages
ainsi que d'enlever les r�sidents d'une cages

	/**
	 * Requete de type POST ajoutant une cage au centre.
     */
    add_cage(Cage cage) throws JAXBException
    
    /**
	 * Requete de type DELETE supprimant l'ensemble des r�sidents d'une cage.
     */
	remove_animals(String cageName) throws JAXBException
	
Nous n'avons pas r�alis� la requ�te permettant de r�cup�rer les informations du trajet depuis une position GPS.
        